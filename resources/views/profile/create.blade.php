@extends('master')

@section('heading')
    <h1 class="h3 mb-0 text-gray-800">Buat Postingan</h1>

@endsection

@section('content')

<div class="ml-3 mt-3 mb-3 mr-3 pt-3">
{{-- <h2>Tambah Data</h2> --}}
    <form action="/medsos" method="POST">
        @csrf
        <div class="form-group">
            <label for="body">text</label>
            <textarea type="text" class="form-control" name="body" id="body" placeholder="Masukkan detail text" rows="3">{{ old('body','')}}</textarea>
            @error('body')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="img">Foto</label>
            <input type="file" id="img" name="img" accept="image/*">
            @error('img')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="caption">Caption</label>
            <input type="text" class="form-control" name="caption" id="caption" placeholder="Masukkan caption" value="{{ old('caption','')}}">
            @error('caption')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="quote">Quote</label>
            <input type="text" class="form-control" name="quote" id="quote" placeholder="Masukkan quote" value="{{ old('quote','')}}">
            @error('quote')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
            @enderror
        </div>
        
    <button type="submit" class="btn btn-primary">Posting</button>
</form>

</div>
@error('body')
<div class=”alert alert-danger”>
    {{ $message }}
</div>
@enderror
</div>

@endsection
