@extends('master')

@section('content')

<div class="mr-3 ml-3 mt-3">
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/posts/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
        @endif
        <div class="form-group">
            <label for="text">text</label>
            <input type="text" class="form-control" name="text" value="{{ old('text', $posts->text)}}" id="text"
                placeholder="Masukkan text">
            @error('text')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="img">Gambar</label>
            <input type="file" id="img" name="img" accept="image/*">
            @error('img')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="caption">caption</label>
            <input type="text" class="form-control" name="caption" value="{{ old('caption', $posts->caption)}}" id="caption"
                placeholder="Masukkan Caption">
            @error('caption')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="quote">quote</label>
            <input type="text" class="form-control" name="quote" value="{{ old('quote', $posts->quote)}}" id="quote"
                placeholder="Masukkan Quote">
            @error('quote')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

@endsection
