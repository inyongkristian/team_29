<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignToAnswerLikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('answer_likes', function (Blueprint $table) {
            $table->foreign('profile_id')
              ->references('id')
              ->on('profiles')->onDelete('cascade');
            $table->foreign('answer_id')
              ->references('id')
              ->on('answers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answer_likes', function (Blueprint $table) {
            //
        });
    }
}
